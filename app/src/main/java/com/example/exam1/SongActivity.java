package com.example.exam1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class SongActivity extends AppCompatActivity {
    private ImageView imgSong;
    private TextView nameSong;
    private TextView bandSong;
    private TextView yearSong;
    private TextView lyricsSong;

    String img, name, band, lyrics;
    short year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);

        imgSong = findViewById(R.id.imgSongDet);
        nameSong = findViewById(R.id.nameSongDet);
        bandSong = findViewById(R.id.bandSongDet);
        yearSong = findViewById(R.id.yearSongDet);
        lyricsSong = findViewById(R.id.lyrucsSongDet);

        getData();
        setData();


    }

    private void getData(){
        if(getIntent().hasExtra("nameSong")){
            img = getIntent().getStringExtra("imgSong");
            name = getIntent().getStringExtra("nameSong");
            band = getIntent().getStringExtra("bandSong");
            year = getIntent().getShortExtra("yearSong", (short) 0);
            lyrics = getIntent().getStringExtra("lyricsSong");
        } else {
            Toast.makeText(this, "No found data!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        Picasso.get().load(img).fit().centerCrop().into(imgSong);
        nameSong.setText(name);
        bandSong.setText(band);
        yearSong.setText(year + "");
        lyricsSong.setText(lyrics);
    }
}