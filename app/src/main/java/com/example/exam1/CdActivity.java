package com.example.exam1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CdActivity extends AppCompatActivity {
    private ImageView imgCDdet;
    private TextView nameCDdet;
    private TextView bandCDdet;
    private TextView infoCDdet;
    List<Song> songs = new ArrayList<>();
    private RecyclerView rViewDet;

    String img, name, band, info;
    List<Song> song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cd);

        imgCDdet = findViewById(R.id.imgCDdetail);
        nameCDdet = findViewById(R.id.nameCDdetail);
        bandCDdet = findViewById(R.id.bandCDdetail);
        infoCDdet = findViewById(R.id.infoBandDetail);
        rViewDet = findViewById(R.id.rViewDetail);

        getData();
        setData();

        MyAdapter2 myAdapter2 = new MyAdapter2(this, song);
        rViewDet.setAdapter(myAdapter2);
        rViewDet.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
    }

    private void getData(){
        if(getIntent().hasExtra("nameCD")){
            img = getIntent().getStringExtra("imgCD");
            name = getIntent().getStringExtra("nameCD");
            band = getIntent().getStringExtra("bandCD");
            info = getIntent().getStringExtra("infoCD");
            song = (List<Song>) getIntent().getExtras().getSerializable("songsCD");
        } else {
            Toast.makeText(this, "No data found!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        Picasso.get().load(img).fit().centerCrop().into(imgCDdet);
        nameCDdet.setText(name);
        bandCDdet.setText(band);
        infoCDdet.setText(info);


    }
}