package com.example.exam1;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder> {

    private Context context;
    private List<Song> songs = new ArrayList<>();

    public MyAdapter2(Context context, List<Song> songs) {
        this.context = context;
        this.songs = songs;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.data_vertical, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(songs.get(position).getImageSong()).fit().centerCrop().into(holder.imgSong);
        holder.nameSong.setText(songs.get(position).getNameSong());
        holder.bandSong.setText(songs.get(position).getBandSong());
        holder.layoutV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, SongActivity.class);
                i.putExtra("imgSong", songs.get(holder.getAdapterPosition()).getImageSong());
                i.putExtra("nameSong", songs.get(holder.getAdapterPosition()).getNameSong());
                i.putExtra("bandSong", songs.get(holder.getAdapterPosition()).getBandSong());
                i.putExtra("yearSong", songs.get(holder.getAdapterPosition()).getYearSong());
                i.putExtra("lyricsSong", songs.get(holder.getAdapterPosition()).getLyrics());
                context.startActivity(i);
            }
        });
        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(holder.clic){
                    holder.oA1.start();
                    holder.play.setImageResource(R.drawable.play2);
                    holder.clic = false;
                } else{
                    holder.oA2.start();
                    holder.play.setImageResource(R.drawable.play);
                    holder.clic = true;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private ImageView imgSong;
        private TextView nameSong;
        private TextView bandSong;
        private ImageView play;
        private ImageView stop;

        ObjectAnimator oA1;
        ObjectAnimator oA2;

        ConstraintLayout layoutV;

        boolean clic = true;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgSong = itemView.findViewById(R.id.imageSong);
            nameSong = itemView.findViewById(R.id.nameSong);
            bandSong = itemView.findViewById(R.id.bandSong);
            layoutV = itemView.findViewById(R.id.layoutV);
            play = itemView.findViewById(R.id.imgPlay);

            oA1 = ObjectAnimator.ofFloat(play, "rotation", 0f, -90f);
            oA2 = ObjectAnimator.ofFloat(play, "rotation", -90f, 0f);
        }
    }
}
