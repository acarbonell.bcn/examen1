package com.example.exam1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter1 extends RecyclerView.Adapter<MyAdapter1.MyViewHolder> {

    private Context context;
    private List<CD> cds = new ArrayList<>();

    public MyAdapter1(Context context, List<CD> cds) {
        this.context = context;
        this.cds = cds;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.data_horitzontal, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(cds.get(position).getImageCD()).fit().centerCrop().into(holder.imgCD);
        holder.nameCD.setText(cds.get(position).getNameCD());
        holder.layoutH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, CdActivity.class);
                i.putExtra("imgCD", cds.get(holder.getAdapterPosition()).getImageCD());
                i.putExtra("nameCD", cds.get(holder.getAdapterPosition()).getNameCD());
                i.putExtra("bandCD", cds.get(holder.getAdapterPosition()).getBandCD());
                i.putExtra("infoCD", cds.get(holder.getAdapterPosition()).getInfoCD());
                i.putExtra("songsCD", (Serializable) cds.get(holder.getAdapterPosition()).getSongs());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cds.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgCD;
        private TextView nameCD;

        ConstraintLayout layoutH;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgCD = itemView.findViewById(R.id.imageCD);
            nameCD = itemView.findViewById(R.id.nameCD);
            layoutH = itemView.findViewById(R.id.layoutH);
        }
    }
}
